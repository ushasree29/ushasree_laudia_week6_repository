package com.greatlearning.assignment6.bean;

import java.sql.Date;
import java.util.Arrays;

public class MoviesComing {
	private int id;
	private String title;
	private int year;
	private String genres;
	private String ratings;
	private String poster;
	private String contentRating;
	private String duration;
	private Date releaseDate;
	private int averageRating;
	private String originalTitle;
	private String storyLine;
	private String actors;
	private String imdbRating;
	private String posterUrl;
	
	public MoviesComing() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MoviesComing(int id, String title, int year, String genres, String ratings, String poster,
			String contentRating, String duration, Date releaseDate, int averageRating, String originalTitle,
			String storyLine, String actors, String imdbRating, String posterUrl) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genres = genres;
		this.ratings = ratings;
		this.poster = poster;
		this.contentRating = contentRating;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.averageRating = averageRating;
		this.originalTitle = originalTitle;
		this.storyLine = storyLine;
		this.actors = actors;
		this.imdbRating = imdbRating;
		this.posterUrl = posterUrl;
	}

	public int getId() {
		return id;
	}

	public  void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getGenres() {
		return genres;
	}

	public void setGenres(String genres) {
		this.genres = genres;
	}

	public String getRatings() {
		return ratings;
	}

	public void setRatings(String ratings) {
		this.ratings = ratings;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getContentRating() {
		return contentRating;
	}

	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getStoryLine() {
		return storyLine;
	}

	public void setStoryLine(String storyLine) {
		this.storyLine = storyLine;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getPosterUrl() {
		return posterUrl;
	}

	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}

	@Override
	public String toString() {
		return "MoviesComing [id=" + id + ", title=" + title + ", year=" + year + ", genres=" + genres
				+ ", ratings=" + ratings + ", poster=" + poster + ", contentRating=" + contentRating
				+ ", duration=" + duration + ", releaseDate=" + releaseDate + ", averageRating=" + averageRating
				+ ", originalTitle=" + originalTitle + ", storyLine=" + storyLine + ", actors="
				+ actors + ", imdbRating=" + imdbRating + ", posterUrl=" + posterUrl + "]";
	}

	
	
}
