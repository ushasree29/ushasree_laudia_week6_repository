package com.greatlearning.assignment6.bean;

import java.sql.Date;

public class Favourit {
private int currentId;
private String title;
private String posterUrl;
private int year;
private Date releaseDate;
private int id;

public Favourit() {
	super();
	// TODO Auto-generated constructor stub
}

public Favourit(int currentId, String title, String posterUrl, int year, Date releaseDate, int id) {
	super();
	this.currentId = currentId;
	this.title = title;
	this.posterUrl = posterUrl;
	this.year = year;
	this.releaseDate = releaseDate;
	this.id = id;
}

public int getCurrentId() {
	return currentId;
}

public void setCurrentId(int currentId) {
	this.currentId = currentId;
}

public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public String getPosterUrl() {
	return posterUrl;
}

public void setPosterUrl(String posterUrl) {
	this.posterUrl = posterUrl;
}

public int getYear() {
	return year;
}

public void setYear(int year) {
	this.year = year;
}

public Date getReleaseDate() {
	return releaseDate;
}

public void setReleaseDate(Date releaseDate) {
	this.releaseDate = releaseDate;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

@Override
public String toString() {
	return "Favourit [currentId=" + currentId + ", title=" + title + ", posterUrl=" + posterUrl + ", year=" + year
			+ ", releaseDate=" + releaseDate + ", id=" + id + "]";
}


}
