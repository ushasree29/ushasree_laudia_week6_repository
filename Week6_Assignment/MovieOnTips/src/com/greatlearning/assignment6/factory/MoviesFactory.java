package com.greatlearning.assignment6.factory;

public class MoviesFactory {
	public static Movies getInstance(String type) {
		if(type.equalsIgnoreCase("moviescoming")) {
			return new MoviesComing();
		}else if(type.equalsIgnoreCase("moviesInTheaters")) {
			return new MoviesInTheaters();
		}else if(type.equalsIgnoreCase("topRatedIndia")) {
			return new TopRatedIndia();
		}else if(type.equalsIgnoreCase("topRatedMovies")) {
			return new TopRatedMovies();
		}else if(type.equalsIgnoreCase("favourit")) {
			return new Favourit();
		}else {
			return null;
		}
	}
}
