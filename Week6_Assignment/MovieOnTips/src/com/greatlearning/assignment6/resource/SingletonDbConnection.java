package com.greatlearning.assignment6.resource;
import java.sql.Connection;
import java.sql.DriverManager;
public class SingletonDbConnection {
private static Connection conn;

private SingletonDbConnection() {
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies_ushasree", "root", "Usha99**");
	    }catch(Exception e) {
		System.out.println("Singleton Db Connection error "+e);
	  }
   }
   public static Connection getDbConnection() {
	   try {
		   Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies_ushasree", "root", "Usha99**");
		   return conn;
	   }catch(Exception e) {
		   System.out.println("Exception caught "+e);
	   }
	   return null;
   }
   public static void closeDbConnection() {
	   try {
		   conn.close();
	       }catch(Exception e) {
		     System.out.println("Connection close properly "+e);
	     }
   }


}
