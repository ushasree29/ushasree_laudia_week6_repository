package com.greatlearning.assignment6.dao;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.greatlearning.assignment6.bean.Favourit;
import com.greatlearning.assignment6.bean.MoviesComing;
import com.greatlearning.assignment6.bean.MoviesInTheaters;
import com.greatlearning.assignment6.bean.TopRatedIndia;
import com.greatlearning.assignment6.bean.TopRatedMovies;
import com.greatlearning.assignment6.resource.SingletonDbConnection;
public class MoviesOnTipsDao {
	public List<MoviesComing> getMovieComingDetails() {
		List<MoviesComing> list=new ArrayList<>();
		try {
			   Connection conn=SingletonDbConnection.getDbConnection();
	           PreparedStatement pstmt = conn.prepareStatement("select *from moviescoming");
	           ResultSet rs=pstmt.executeQuery();
	           while(rs.next()) {
	        	 MoviesComing mc=new MoviesComing();
	        	 mc.setId(rs.getInt(1));
	        	 mc.setTitle(rs.getString(2));
	        	 mc.setYear(rs.getInt(3));
	        	 //Array strGenre=rs.getArray("genres");
	        	// String genres[]=(String[])strGenre.getArray();
	        	 mc.setGenres(rs.getString(4));
	        	 //Array strRating=rs.getArray(5);
	        	 //int ratings[]=(int[])strRating.getArray();
	        	 mc.setRatings(rs.getString(5));
	        	 mc.setPoster(rs.getString(6));
	        	 mc.setContentRating(rs.getString(7));
	        	 mc.setDuration(rs.getString(8));
	        	 mc.setReleaseDate(rs.getDate(9));
	        	 mc.setAverageRating(rs.getInt(10));
	        	 mc.setOriginalTitle(rs.getString(11));
	        	 mc.setStoryLine(rs.getString(12));
	        	 //Array strActors=rs.getArray("Actors");
	        	 //String actors[]=(String[])strActors.getArray();
	        	 mc.setActors(rs.getString(13));
	        	 mc.setImdbRating(rs.getString(14));
	        	 mc.setPosterUrl(rs.getString(15));
	        	 list.add(mc);
	        	 }
              
            }catch (Exception e) {
		       System.out.println("In store method "+e);
		      
	        }
		return list;
    }
	public List<MoviesInTheaters> getMovieInTheatersDetails() {
		List<MoviesInTheaters> list=new ArrayList<>();
		try {
			   Connection conn=SingletonDbConnection.getDbConnection();
	           PreparedStatement pstmt = conn.prepareStatement("select *from moviesintheaters");
	           ResultSet rs=pstmt.executeQuery();
	           while(rs.next()) {
	        	 MoviesInTheaters mc=new MoviesInTheaters();
	        	 mc.setId(rs.getInt(1));
	        	 mc.setTitle(rs.getString(2));
	        	 mc.setYear(rs.getInt(3));
	        	 //Array strGenre=rs.getArray("genres");
	        	 //String genres[]=(String[])strGenre.getArray();
	        	 mc.setGenres(rs.getString(4));
	        	 //Array strRating=rs.getArray(5);
	        	 //int ratings[]=(int[])strRating.getArray();
	        	 mc.setRatings(rs.getString(5));
	        	 mc.setPoster(rs.getString(6));
	        	 mc.setContentRating(rs.getString(7));
	        	 mc.setDuration(rs.getString(8));
	        	 mc.setReleaseDate(rs.getDate(9));
	        	 mc.setAverageRating(rs.getInt(10));
	        	 mc.setOriginalTitle(rs.getString(11));
	        	 mc.setStoryLine(rs.getString(12));
	        	 //Array strActors=rs.getArray("Actors");
	        	 //String actors[]=(String[])strActors.getArray();
	        	 mc.setActors(rs.getString(13));
	        	 mc.setImdbRating(rs.getString(14));
	        	 mc.setPosterUrl(rs.getString(15));
	        	 list.add(mc);
	           }
            }catch (Exception e) {
		       System.out.println("In store method "+e);
		      
	        }
		return list;
    }
	public List<TopRatedIndia> getTopRatedIndiaMoviesDetails() {
		List<TopRatedIndia> list=new ArrayList<>();
		try {
			   Connection conn=SingletonDbConnection.getDbConnection();
	           PreparedStatement pstmt = conn.prepareStatement("select *from topratedindia");
	           ResultSet rs=pstmt.executeQuery();
	           while(rs.next()) {
	        	 TopRatedIndia mc=new TopRatedIndia();
	        	 mc.setTitle(rs.getString(1));
	        	 mc.setYear(rs.getInt(2));
	        	 //Array strGenre=rs.getArray("genres");
	        	 //String genres[]=(String[])strGenre.getArray();
	        	 mc.setGenres(rs.getString(3));
	        	 //Array strRating=rs.getArray(4);
	        	 //int ratings[]=(int[])strRating.getArray();
	        	 mc.setRatings(rs.getString(4));
	        	 mc.setPoster(rs.getString(5));
	        	 mc.setContentRating(rs.getString(6));
	        	 mc.setDuration(rs.getString(7));
	        	 mc.setReleaseDate(rs.getDate(8));
	        	 mc.setAverageRating(rs.getInt(9));
	        	 mc.setOriginalTitle(rs.getString(10));
	        	 mc.setStoryLine(rs.getString(11));
	        	 //Array strActors=rs.getArray("Actors");
	        	 //String actors[]=(String[])strActors.getArray();
	        	 mc.setActors(rs.getString(12));
	        	 mc.setImdbRating(rs.getString(13));
	        	 mc.setPosterUrl(rs.getString(14));
	        	 list.add(mc);
	           }
            }catch (Exception e) {
		       System.out.println("In store method "+e);
		       
	        }
		return list;
    }
	public List<TopRatedMovies> getTopRatedMoviesDetails() {
		List<TopRatedMovies> list=new ArrayList<>();
		try {
			   Connection conn=SingletonDbConnection.getDbConnection();
	           PreparedStatement pstmt = conn.prepareStatement("select *from topratedmovies");
	           ResultSet rs=pstmt.executeQuery();
	           while(rs.next()) {
	        	 TopRatedMovies mc=new TopRatedMovies();
	        	 mc.setTitle(rs.getString(1));
	        	 mc.setYear(rs.getInt(2));
	        	 //Array strGenre=rs.getArray("genres");
	        	 //String genres[]=(String[])strGenre.getArray();
	        	 mc.setGenres(rs.getString(3));
	        	 //Array strRating=rs.getArray(4);
	        	 //int ratings[]=(int[])strRating.getArray();
	        	 mc.setRatings(rs.getString(4));
	        	 mc.setPoster(rs.getString(5));
	        	 mc.setContentRating(rs.getString(6));
	        	 mc.setDuration(rs.getString(7));
	        	 mc.setReleaseDate(rs.getDate(8));
	        	 mc.setAverageRating(rs.getInt(9));
	        	 mc.setOriginalTitle(rs.getString(10));
	        	 mc.setStoryLine(rs.getString(11));
	        	 //Array strActors=rs.getArray("Actors");
	        	 //String actors[]=(String[])strActors.getArray();
	        	 mc.setActors(rs.getString(12));
	        	 mc.setImdbRating(rs.getString(13));
	        	 mc.setPosterUrl(rs.getString(14));
	        	 list.add(mc);
	        	 
	           }
            }catch (Exception e) {
		       System.out.println("In store method "+e);
		       
	        }
		return list;
    }
	public List<Favourit> getFavouritMoviesDetails() {
		List<Favourit> list=new ArrayList<>();
		try {
			   Connection conn=SingletonDbConnection.getDbConnection();
	           PreparedStatement pstmt = conn.prepareStatement("select *from favourit");
	           ResultSet rs=pstmt.executeQuery();
	           while(rs.next()) {
	        	 Favourit mc=new Favourit();
	        	 mc.setCurrentId(rs.getInt(1));
	        	 mc.setTitle(rs.getString(2));
	        	 mc.setPosterUrl(rs.getString(3));
	        	 mc.setYear(rs.getInt(4));
	        	 mc.setReleaseDate(rs.getDate(5));
	        	 mc.setId(rs.getInt(6));
	        	 list.add(mc);
	           }
            }catch (Exception e) {
		       System.out.println("In store method "+e);
		      
	        }
		return list;
    }
	
	
}
