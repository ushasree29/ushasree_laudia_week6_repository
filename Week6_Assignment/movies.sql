create database movies_ushasree;

use movies_ushasree;

create table moviescoming(
     id int primary key,
     title varchar(30),
     year int,
     genres varchar(40),
     ratings varchar(20),
     poster varchar(150),
     contentRating varchar(10),
     duration varchar(10),
     releaseDate date,
     averageRating int,
     originalTitle varchar(40),
     storyLine varchar(150),
     actors varchar(50),
     imdbRating varchar(10),
     posterUrl varchar(150)
     );

insert into moviescoming values(1,"Game Night",2018,'Action,Comedy,Crime','10,4,6,2,7',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",11,"PT100M",'2018-02-28',0,"",
"A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.",'Rachel,Jesse,Jason',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviescoming values(2,"Area X: Annihilation",2018,'Adventure,Drama,Fantasy','2,6,5,6,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",12,"",'2018-02-23',0,"Annihilation",
"A biologist's husband disappears. The expedition team is made up of the biologist, an anthropologist, a psychologist, a surveyor, and a linguist.",
'Tessa,Jennifer,Natalie',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviescoming values(3,"Hannah",2017,'Drama','2,6,5,6,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",12,"PT95M",'2018-01-24',0,"",
"Intimate portrait of a woman drifting between reality and denial when consequences of her husband's imprisonment.",
'Charlotte,Andre,Stephanie',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviescoming values(4,"The Lodgers",2017,'Drama,Horror,Romance','3,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","R","PT92M",'2018-03-09',0,"",
"1920, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Vega,David,Moe',5.8,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviescoming values(5,"Beast of Burden",2018,'Action,Comedy,Crime','9,10,5,6,3,7,9',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","R","PT100M",'2018-02-01',0,"",
"Sean Haggerty only has an hour to deliver his illegal cargo. And he must do it all from the cockpit of his Cessna.",'Daniel,Grace,Pablo',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

select *from moviescoming;

create table moviesintheaters(
     id int primary key,
     title varchar(30),
     year int,
     genres varchar(40),
     ratings varchar(30),
     poster varchar(150),
     contentRating varchar(10),
     duration varchar(10),
     releaseDate date,
     averageRating int,
     originalTitle varchar(40),
     storyLine varchar(150),
     actors varchar(40),
     imdbRating varchar(10),
     posterUrl varchar(150)
     );

insert into moviesintheaters values(1,"Hannah",2017,'Drama','2,6,5,6,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",12,"PT95M",'2018-01-24',0,"",
"Intimate portrait of a woman drifting between reality and denial when consequences of her husband's imprisonment.",
'Charlotte,Andre,Stephanie',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviesintheaters values(2,"Game Night",2018,'Action,Comedy,Crime','2,10,5,6,3,7,9',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",11,"PT100M",'2018-02-28',0,"",
"A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.",'Rachel,Jesse,Jason',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviesintheaters values(3,"Area X: Annihilation",2018,'Adventure,Drama,Fantasy','2,6,5,6,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",12,"",'2018-02-23',0,"Annihilation",
"A biologist's husband disappears. The expedition team is made up of the biologist, an anthropologist, a psychologist, a surveyor, and a linguist.",
'Tessa,Jennifer,Natalie',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviesintheaters values(4,"Beast of Burden",2018,'Action,Comedy,Crime','9,10,5,6,3,7,9',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","R","PT100M",'2018-02-01',0,"",
"Sean Haggerty only has an hour to deliver his illegal cargo. And he must do it all from the cockpit of his Cessna.",'Daniel,Grace,Pablo',"",
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviesintheaters values(5,"The Lodgers",2017,'Drama,Horror,Romance','3,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","R","PT92M",'2018-03-09',0,"",
"1920, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Vega,David,Moe',5.8,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

select *from moviesintheaters;

create table topratedindia(
     title varchar(30) primary key,
     year int,
     genres varchar(40),
     ratings varchar(40),
     poster varchar(150),
     contentRating varchar(10),
     duration varchar(10),
     releaseDate date,
     averageRating int,
     originalTitle varchar(40),
     storyLine varchar(150),
     actors varchar(40),
     imdbRating varchar(10),
     posterUrl varchar(150)
     );

insert into topratedindia values("Anand",1971,'Drama','3,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT161M",'1971-03-12',0,"",
"1971, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Rajesh,Amitabh,Sumita',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedindia values("Dangal",2016,'Action,Biography,Drama','7,6,5,5,3,7,1',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT122M",'2016-12-23',0,"",
"2016, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Aamir khan,Sakshi Tanwar,Fatima Sana',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedindia values("Drishyam",2013,'Crime,Drama','8,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT160M",'2013-12-19',0,"",
"2013, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Mohancal,Meena,Ansiba',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedindia values("Nayakan",1987,'Drama,Crime','7,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT145M",'1987-07-31',0,"",
"1987, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'kamal Haasan,Saranya,Ganesh',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedindia values("Anbe Sivan",2003,'Comedy,Adventure,Drama','8,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT160M",'2003-01-14',0,"",
"2003, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Kamal Haasan,Madhavan,kiran',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

select *from topratedindia;

create table topratedmovies(
     title varchar(30) primary key,
     year int,
     genres varchar(30),
     ratings varchar(40),
     poster varchar(150),
     contentRating varchar(10),
     duration varchar(10),
     releaseDate date,
     averageRating int,
     originalTitle varchar(40),
     storyLine varchar(150),
     actors varchar(40),
     imdbRating varchar(10),
     posterUrl varchar(150)
     );


insert into topratedmovies values("Baazigar",1993,'Crime,Drama,Musical','3,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT161M",'1993-11-12',0,"",
"1993, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Shah RukhKhan,Rakhee,Kajol',7.8,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedmovies values("24",2016,'Action,Biography,Romance','7,6,5,5,3,7,1',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT122M",'2016-12-23',0,"",
"2016, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Suriya,Samantha,Nithya menon',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedmovies values("Jodha Akbar",2016,'Crime,Drama','8,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT160M",'2016-12-19',0,"",
"2016, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Hritik Roshan,Aishwarya,Sonu Sood',7.6,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedmovies values("Wake up Sid",2008,'Drama,Crime','7,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT145M",'2008-07-31',0,"",
"2008, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Ranbir Kapoor,Supriya,Ganesh',8.9,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into topratedmovies values("Saala Khadoos",2016,'Comedy,Adventure,Drama','8,6,5,5,3,7,10',
"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","","PT160M",'2016-01-14',0,"",
"2016, rural Ireland. Anglo Irish twins Rachel and Edward. Each night, the property  the threshold; if  is placed in jeopardy.",
'Nassar,Madhavan,Radha Ravi',7.8,
"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

select *from topratedmovies;


create table favourit(
currentId int primary key,title varchar(30),posterUrl varchar(150),year int,releaseDate date,id int);

insert into favourit values(
1,"Game Night","https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",
2018,'2018-02-28',1);

insert into favourit values(
2,"Area X: Annihilation","https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",
2018,'2018-02-23',2);

insert into favourit values(
3,"Hannah","https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",
2017,'2017-02-28',3);

insert into favourit values(
4,"The Lodgers","https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",
2018,'2018-03-09',4);

insert into favourit values(
5,"Beast of Burden","https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg",
2018,'2018-02-14',5);

select *from favourit;
