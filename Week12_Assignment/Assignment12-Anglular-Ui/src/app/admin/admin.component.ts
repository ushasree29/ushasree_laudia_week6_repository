import { Component, OnInit } from '@angular/core';
import { User , loginUser, loginAdmin} from '../user';
import { Book } from '../book';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  books:Array<Book>=[];
  storemsg:string="";
   bookid:number=0;
   bookname:string="";
   image:string="";
 booktype:string=""
   authorname:string="";
   msg:string="";
deleteMsg:string="";
flag:boolean=false;
public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
updateMsg:string="";
admin= new loginAdmin();
  constructor( public pser:UserService, public  router:Router) { }

  ngOnInit(): void {
    this.loadProducts();
  }
  loadProducts(): void{
    // console.log("event fired")
    this.pser.loadProductDetails().subscribe(res=>this.books=res);
   // this.pser.loadProductDetails();
   }
   deleteProduct(pid:number)
{
  //console.log(pid)
  this.pser.deleteProductDetails(pid).subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadProducts());
}
updateProducts(updateRef:NgForm)
{
  console.log(updateRef.value);
}
updateProduct(product:Book)
{
  console.log(product);
  this.flag=true;
  this.bookid=product.bookid;
  this.booktype=product.booktype;
  this.bookname=product.bookname;
  this.authorname=product.authorname;

}
updateProductdetails()
{
  let product={"bookid":this.bookid,"booktype":this.booktype,"bookname":this.bookname,"authorname":this.authorname}
 // console.log(product);
  this.pser.updateProductinfo(product).subscribe(result=>this.updateMsg,error=>console.log(error),
  ()=>{
    this.loadProducts();
    this.flag=false;
  }
  );
}
Hide()
{
  this.hide=!this.hide;
  if(this.hide)
  {
    
    this.nameT= "Hide";

  }
  else{
    this.nameT = "Show";
  }
}
storeProduct(productRef:NgForm)
{
  //console.log(productRef.value)
  this.pser.storeAdminDetails(productRef.value).subscribe(res=>this.storemsg=res,error=>console.log(error));
  
}
loginUser()
{
this.pser.loginUserFromRemote(this.admin).subscribe(
  data =>{ 
    this.router.navigate(['/menu'])  
   },
  error => {console.log("exception occured");
this.msg="Bad credentials,please add proper credentials";
}
)
}
}
