import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from './book';
import { User , Admin } from './user';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http:HttpClient) { }
  loadProductDetails():Observable<Book[]>
  {
   //console.log("service class");
 return  this.http.get<Book[]>("http://localhost:9090/admin/books/getBooks")
    //subscribe(res=>console.log(res),error=>console.log(error),()=>console.log("compplted"));
   // this.http.get("http://localhost:9090/admin/books/getBooks")
  }

storeProductDetails(product:User):Observable<string>
  {
    return this.http.post("http://localhost:9090/admin/users/addUser",product,{responseType:'text'})
  }
//    loginUserFromRemote(user:User):Observable<string>
//   {
//  return this.http.post<string>("http://localhost:9090/admin/users/userlogin",user,{responseType:'text'})
//   }
loginUserFromRemote(product:User):Observable<string>
  {
    return this.http.post("http://localhost:9090/admin/users/userlogin",product,{responseType:'text'})
  }
  storeBookDetails(product:Book):Observable<string>
  {
    return this.http.post("http://localhost:9090/admin/books/addbooks",product,{responseType:'text'})
  }
  deleteProductDetails(bookid:number):Observable<string>
  {
    return this.http.delete("http://localhost:9090/admin/books/delete/"+bookid,{responseType:'text'})
  }
  updateProductinfo(product:any):Observable<string>
  {
    return this.http.patch("http://localhost:9090/admin/books/bookupdate",product,{responseType:'text'})
  }
  storeAdminDetails(product:Admin):Observable<string>
  {
    return this.http.post("http://localhost:9090/admins/addAdmin",product,{responseType:'text'})
  }
  loginAdminFromRemote(product:Admin):Observable<string>
  {
    return this.http.post("http://localhost:9090/admins/login",product,{responseType:'text'})
  }
  loadUserDetails():Observable<User[]>
  {
   //console.log("service class");
 return  this.http.get<User[]>("http://localhost:9090/admin/users/getUsers")
    //subscribe(res=>console.log(res),error=>console.log(error),()=>console.log("compplted"));
   // this.http.get("http://localhost:9090/admin/books/getBooks")
  }
  deleteUsersDetails(email:string):Observable<string>
  {
    return this.http.delete("http://localhost:9090/admin/users/deleteuser"+email,{responseType:'text'})
  }
  updateUserinfo(user:any):Observable<string>
  {
    return this.http.patch("http://localhost:9090/admin/users/userUpdate",user,{responseType:'text'})
  }
}