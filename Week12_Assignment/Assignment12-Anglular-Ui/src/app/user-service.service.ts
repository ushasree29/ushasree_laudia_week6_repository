import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from './book';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http:HttpClient) { }
  loadProductDetails():Observable<Book[]>
  {
   // console.log("service class");
  return   this.http.get<Book[]>("http://localhost:9090/admin/books/getBooks")
    //subscribe(res=>console.log(res),error=>console.log(error),()=>console.log("compplted"));
    
  }

  // storeProductDetails(product:Book):Observable<string>
  // {
  //   return this.http.post("http://localhost:9090/admin/books/addbooks",product,{responseType:'text'})
  // }
}
