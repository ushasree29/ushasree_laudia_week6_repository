package com.greateleaning.assignment9.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greateleaning.assignment9.bean.Admin;
import com.greateleaning.assignment9.bean.User;
import com.greateleaning.assignment9.dao.AdminDao;

@Service
public class AdminService {
@Autowired
AdminDao adminDao;
public Admin login(String email,String password)
{
	Admin admin = adminDao.findByEmailAndPassword(email, password);
	return admin;
}
public String addAdminINfo(Admin admin )
{
	if(adminDao.existsById(admin.getEmail()))
	{
		return "this email id already present present";
	}
	else
	{
		adminDao.save(admin);
		return " User Information store";
	}
}
}
