package com.greateleaning.assignment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greateleaning.assignment9.bean.Book;
import com.greateleaning.assignment9.service.BookService;

@RestController
@RequestMapping("/admin/books")
@CrossOrigin
public class BookController {
@Autowired
BookService bookService;
@PostMapping(value="addbooks", consumes = MediaType.APPLICATION_JSON_VALUE)
public String insertBooks(@RequestBody Book book)
{
	return bookService.addBookINfo(book);
}
@GetMapping(value="getBooks",produces = MediaType.APPLICATION_JSON_VALUE)
public List<Book> getAllBookInfo()
{
	return bookService.getAllBooks();
}
@DeleteMapping(value = "delete/{bookid}" )
public String deleteData(@PathVariable("bookid")  int bookid)
{
	return bookService.deleteBook(bookid);
}

@PatchMapping(value="bookupdate",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String updateDetails(@RequestBody Book book)	
{
return bookService.updateBook(book);
}

}
