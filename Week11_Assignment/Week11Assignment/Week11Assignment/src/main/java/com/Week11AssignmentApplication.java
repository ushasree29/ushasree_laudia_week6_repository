package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Week11AssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week11AssignmentApplication.class, args);
		System.out.println("running on port no: 8761");
	}

}
