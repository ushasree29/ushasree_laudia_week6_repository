package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.greatlearning.assignment11.bean.Book;
import com.greatlearning.assignment11.controller.BookController;
import com.greatlearning.assignment11.dao.BookDao;
import com.greatlearning.assignment11.service.BookService;
@SpringBootTest
class BookControllerTest {

	@Autowired
	BookController bc;
	
	@Mock
	BookDao dao;
	
	@InjectMocks
	BookService service;
	
	@Test
	void testGetAllBookDetails() {
		//fail("Not yet implemented");
		List<Book> list=bc.getAllBookInfo();
		assertTrue(true);
	}

	@Test
	void testStoreBookInfo() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.getAuthor();
		b.getName();
		b.getPrice();
		b.getType();
		b.getId();
	    String res=bc.storeBook(b);
	    assertTrue(true);
	}

	@Test
	void testDeleteBookInfo() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.getId();
		String res=bc.deleteBooks(1);
		assertTrue(true);
	}

	@Test
	void testUpdateBookInfo() {
		//fail("Not yet implemented");
		Book b = new Book();
		when(dao.findById(b.getId())).thenReturn(Optional.of(b));
		service.updateBookInfo(b);
		Book b1 = new Book();
		assertEquals(0,b1.getPrice());
	}

}
