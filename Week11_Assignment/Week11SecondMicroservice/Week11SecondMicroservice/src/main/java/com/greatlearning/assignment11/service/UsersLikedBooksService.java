package com.greatlearning.assignment11.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment11.bean.Book;
import com.greatlearning.assignment11.bean.UsersLikedBooks;
import com.greatlearning.assignment11.dao.UsersLikedBooksDao;

@Service
public class UsersLikedBooksService {

	@Autowired
	UsersLikedBooksDao usersLikedBooksdao;
	
	public List<UsersLikedBooks> getAllBooks(){
  	  return usersLikedBooksdao.findAll();
    }
    
    public String storeBookInfo(UsersLikedBooks uld) {
  	  if(usersLikedBooksdao.existsById(uld.getId())) {
  		  return "book id should be unique";
  	  }else {
  		usersLikedBooksdao.save(uld);
  		  return "book saved successfully";
  	  }
    }
}
