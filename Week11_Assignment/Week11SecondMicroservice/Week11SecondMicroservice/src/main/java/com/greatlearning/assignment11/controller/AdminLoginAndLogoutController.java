package com.greatlearning.assignment11.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.assignment11.service.AdminLoginAndLogoutService;
import com.greatlearning.assignment11.bean.AdminLoginAndLogout;
import com.greatlearning.assignment11.bean.AdminSignUp;

@RestController
@RequestMapping(value="/adminloggs")
public class AdminLoginAndLogoutController {

	@Autowired
	AdminLoginAndLogoutService adminloginandlogoutService;
	
	@GetMapping(value="getAllLogin",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<AdminLoginAndLogout> getAllAdminInfo(){
		 return adminloginandlogoutService.getAllAdmin();
	 }
	 
	 @PostMapping(value="storeLogin",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeAdmin(@RequestBody AdminLoginAndLogout ad) {
		 return adminloginandlogoutService.storeAdminInfo(ad);
	 }
	 
	 @DeleteMapping(value="Logout/{id}")
	 public String deleteAdmin(@PathVariable("id")int id) {
		 return adminloginandlogoutService.deleteLoginInfo(id);
		}
}
