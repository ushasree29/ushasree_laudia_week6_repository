package com.greatlearning.assignment11.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment11.bean.AdminLoginAndLogout;
import com.greatlearning.assignment11.bean.AdminSignUp;
import com.greatlearning.assignment11.dao.AdminLoginAndLogoutDao;

@Service
public class AdminLoginAndLogoutService {

	@Autowired
	AdminLoginAndLogoutDao adminloginandlogoutDao;
	
	public List<AdminLoginAndLogout> getAllAdmin(){
	  	  return adminloginandlogoutDao.findAll();
	    }
	    
	    public String storeAdminInfo(AdminLoginAndLogout ad) {
	  	  if(adminloginandlogoutDao.existsById(ad.getId())) {
	  		  return "enter correct password";
	  	  }else {
	  		adminloginandlogoutDao.save(ad);
	  		  return "admin login information saved successfully";
	  	  }
	    }
	    public String deleteLoginInfo(int id) {
      	  if(!adminloginandlogoutDao.existsById(id)){
      		  return "id not present enter correct id";
      	  }else {
      		
      		  return "Logged out..!!!";
      	  }
        }
}

//adminloginandlogoutDao.deleteById(id);
//  return "Logged out..!!!";
