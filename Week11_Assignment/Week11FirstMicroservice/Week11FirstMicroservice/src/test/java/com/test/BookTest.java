package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.greatlearning.assignment11.bean.Book;

class BookTest {

	@Test
	void testGetId() {
		//fail("Not yet implemented");
		Book b=new Book();
		int res=b.getId();
		assertEquals(0, res);
	}

	@Test
	void testSetId() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.setId(1);
		assertTrue(true);
		
	}

	@Test
	void testGetName() {
		//fail("Not yet implemented");
		Book b=new Book();
		String res1=b.getName();
		assertEquals(null,res1);
		
	}

	@Test
	void testSetName() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.setName("Game Night");
		assertEquals("Game Night","Game Night");
		
	}

	@Test
	void testGetAuthor() {
		//fail("Not yet implemented");
		Book b=new Book();
		String res2=b.getAuthor();
		assertEquals(null,res2);
	}

	@Test
	void testSetAuthor() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.setAuthor("Game Night");
		assertEquals("Game Night","Game Night");
		
	}

	@Test
	void testGetType() {
		//fail("Not yet implemented");
		Book b=new Book();
		String res3=b.getType();
		assertEquals(null,res3);
	}

	@Test
	void testSetType() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.setType("Genre");
		assertEquals("Genre","Genre");
	}

}
