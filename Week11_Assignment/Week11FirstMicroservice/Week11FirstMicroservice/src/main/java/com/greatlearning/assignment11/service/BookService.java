package com.greatlearning.assignment11.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment11.bean.Book;
import com.greatlearning.assignment11.dao.BookDao;

@Service
public class BookService {
        @Autowired
        BookDao bookDao;
        
        public List<Book> getAllBooks(){
        	return bookDao.getAllBooks();
        }
        
        public Book getOneById(int id) {
        	return bookDao.getOne(id);
        }
}

