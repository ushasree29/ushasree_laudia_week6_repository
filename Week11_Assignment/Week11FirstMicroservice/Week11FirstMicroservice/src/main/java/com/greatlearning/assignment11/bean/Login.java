package com.greatlearning.assignment11.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Login {
@Id
private String emailId;
private String password;
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
@Override
public String toString() {
	return "Login [emailId=" + emailId + ", password=" + password + "]";
}


}