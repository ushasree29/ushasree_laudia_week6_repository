package com.greatlearning.miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.dao.*;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
	public String verifyPassword(Admin admin) {
		if (adminDao.existsById(admin.getEmail())) {
			Admin a = adminDao.getById(admin.getEmail());

			if (a.getPassword().equals(admin.getPassword())) {
				return "Admin Mr/Mrs. " + admin.getEmail() + " Login Successful";
			} 
			else {
				return "Admin Mr/Mrs. " + admin.getEmail() + " Login Failed! Wrong Credentials.";
			}
		} 
		else {
			return "Admin Mr/Mrs. " + admin.getEmail() + " Login Failed! You are not Admin!!";

		}

	}
}
