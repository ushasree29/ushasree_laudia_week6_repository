package com.greatlearning.miniproject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.greatlearning.miniproject.bean.*;

public interface OrdersDao extends JpaRepository<Orders, CompositeKeyForOrder> {

	@Query("select o from Orders o where o.key.email=:email ")
	public List<Orders> getAllOrders(@Param("email") String email);



}
