package com.greatlearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.miniproject.bean.*;

@Repository
public interface AdminDao extends JpaRepository<Admin, String> {

}
