package com.greatlearning.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.service.*;

@Controller
@RequestMapping("/menu")
public class MenuControllerForAdmin {
	@Autowired
	MenuService menuService;

	@GetMapping(value = "addNewMenuPage")
	public String openAddMenuPage() {
		return "addNewMenu";
	}

	@PostMapping(value = "addNewMenu")
	public String addNewMenu(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		float price = Float.parseFloat(request.getParameter("price"));
		String addMenuResult = menuService.addNewMenu(new Menu(id, name, price));
		request.setAttribute("objAddMenu", addMenuResult);
		return "addNewMenu";
	}

	@GetMapping(value = "displayMenuForAdmin")
	public String openDisplayAllMenu(HttpServletRequest request) {
		List<Menu> listOfMenu = menuService.getAllMenu();
		request.setAttribute("objMenu", listOfMenu);
		return "displayMenuForAdmin";
	}

	@GetMapping(value = "updateItemPricePage")
	public String openUpdateItemPricePage() {
		return "updateItemPrice";
	}

	@PostMapping(value = "updateItemPrice")
	public String UpdateItemPrice(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		float price = Float.parseFloat(request.getParameter("price"));
		Menu item = new Menu();
		item.setItemId(id);
		item.setItemPrice(price);
		String updateResult = menuService.updateItemPrice(item);
		request.setAttribute("objUpdateIPrice", updateResult);
		return "updateItemPrice";
	}

	@GetMapping(value = "deleteItemPage")
	public String openDeleteItemPage() {
		return "deleteItem";
	}

	@PostMapping(value = "deleteItem")
	public String deleteItem(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		String deleteResult = menuService.deleteItem(id);
		request.setAttribute("objDeleteItem", deleteResult);
		return "deleteItem";
	}

}
