package com.greatlearning.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.dao.*;

@Service
public class MyCartService {
	@Autowired
	MyCartDao cartDao;

	public String addToCart(MyCart cart) {
		if (cartDao.existsById(cart.getKey())) {
			return "Item already in the cart";
		} else {
			cartDao.save(cart);
			return "Item added to cart - " + cart.getItemName() + ".";
		}

	}

	public List<MyCart> getMyCart(String emial) {

		return cartDao.getMyCart(emial);
	}

	public float getMyTotal(String email) {
		return cartDao.getMyTotal(email);
	}

	public String deleteItem(CompositeKeyForCart key) {
		System.out.println(cartDao.getById(key));
		cartDao.deleteById(key);
		return "Item Deleted.";
	}

	public String deleteAll(String email) {
		cartDao.deleteAll(email);
		return "Thank You for Ordering!!";
	}

}
