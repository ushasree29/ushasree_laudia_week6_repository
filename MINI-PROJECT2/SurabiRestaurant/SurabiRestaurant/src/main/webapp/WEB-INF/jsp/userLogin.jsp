
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Log In</title>

<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }      
</style>

</head>
<body>

	<div align="center">
		<h1>SURABI RESTAURANT</h1>
		<hr>
		<h2>User Login</h2>
		<form action="/user/userLogin" method="post">
			<label> E-Mail &nbsp &nbsp </label> <input type="email" name="email" required>
			<br />
			<br /> <label>Password </label> <input type="password" name="password" required><br />
			<br /> <input type="submit" value="Submit" class ="btn"> 
			<br />
			<br/>
			<hr>
		</form>
		New User..?
		<a href="/user/signinPage"><button class ="btn">Registration</button>
		</a>
		<hr>
		Status:
	<%
	Object loginResult = request.getAttribute("objLogInResult");
	if (loginResult != null) {
		out.print(loginResult);
	}
	%>
	</div>
	
</body>
</html>