<%@page import="java.util.Iterator"%>
<%@page import="com.greatlearning.miniproject.bean.MyCart"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Cart</title>
<style type="text/css">
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
         table {
        transform: translate(-0%, -5%);
        border-collapse: collapse;
        width: 500px;
        height: 10px;
        border: 1px solid #bdc3c7;
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    } 
     tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }  
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }           
</style>
</head>
<body>

	<%Object name = session.getAttribute("objName");
	
	Object result = request.getAttribute("objDelete");
	if(result!=null){
	out.print("<br>"+result);
	}
	%>

	<div align="center">
		
		<h1>SURABI RESTAURANT</h1>
		<hr>
		<div align="center">
			<a href="/user/userHome"><button class="btn1">Home</button></a>
			<a href="/user/userlogout"><button class="btn">Logout</button>
			<br></a>
		<hr>
		</div>

<%
Object cartList= request.getAttribute("objCart");

Object total = request.getAttribute("objTotal");

if(cartList==null){
	out.print("<br/><h3>Your Cart is Empty <h3>");
}
else{
	List<MyCart> listOfItems = (List<MyCart>)cartList;
	if(listOfItems.isEmpty()){
		out.print("<br/><h3>Your Cart is Empty <h3>");
	}
	else{
		out.print("<br/><h3><h3>");
		Iterator<MyCart> ii = listOfItems.iterator();
		int slNo=1;
		
		%>

	<h2>Your Order</h2>
	<table>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Plates</th>
			<th>Amount</th>
			<th>Remove</th>
		</tr>

		<%
			while(ii.hasNext()){
				MyCart cart = ii.next();
				%>
		<tr>
			<td><%=cart.getKey().getItemId() %></td>
			<td><%=cart.getItemName() %></td>
			<td><%= cart.getNumberOfPlates()%></td>
			<td><%=cart.getTotalPrice() %></td>
			<td>
				<form action="deleteItem" method="post">
					<input type="hidden" name="id"
						value="<%=cart.getKey().getItemId() %>" /> <input type="submit"
						value="Delete" class="btn2">
				</form>
			</td>
		</tr>

		<%
				
			}
		
		
		%>

	</table>
	<div class ="div">
	<h3>
		Total Amount = <%=total %>/-
	</h3></div>

<hr>
	<form action="/order/generateBill" method="post">
		<%session.setAttribute("objCartList", cartList);  %>
		<input type="submit" value="Place Order >" class="btn">
	</form>


	<%
	}
}
%>
</div>
</body>
</html>