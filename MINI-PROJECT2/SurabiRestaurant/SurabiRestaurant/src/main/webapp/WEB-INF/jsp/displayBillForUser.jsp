<%@page import="java.util.Iterator"%>
<%@page import="com.greatlearning.miniproject.bean.Orders"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bill</title>
<style>
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
         table {
        transform: translate(-0%, -5%);
        border-collapse: collapse;
        width: 500px;
        height: 10px;
        border: 1px solid #bdc3c7;
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    } 
     tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }  
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }      
</style>
</head>
<body>
	<%
	Object name = session.getAttribute("objName");
	%>
	<div align="center">
		<h1>SURABI RESTAURANT</h1><hr>
		<h2>-- Surabi Restaurant Bill --</h2>
		
	<%
	Object obj = session.getAttribute("objBill");
	List<Orders> listOfOrders = (List<Orders>) obj;
	if (!listOfOrders.isEmpty()) {
		Iterator ii = listOfOrders.iterator();
		float total = 0;
		String dateAndTime = null;
		int slNo = 1;
	%>

	<table>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Plates</th>
			<th>Amount</th>

		</tr>
		<%
		while (ii.hasNext()) {
			Object object = ii.next();
			Orders o = (Orders) object;
			dateAndTime = o.getKey().getDateAndTime();
			total += o.getTotalPrice();
		%>
		<tr>
			<td><%=o.getKey().getItemId()%></td>
			<td><%=o.getItemName()%></td>
			<td><%=o.getNumberOfPlate()%></td>
			<td><%=o.getTotalPrice()%></td>
		</tr>
		<%
		}
		%>
	</table>
	<h3>
		Items Total = <%=total%>/- <br>
		Delivery Charge = 40.0/-<br>
		Taxes = 25.0/-<br>
		-------------------------------<br>
		Grand Total = <%=total+40+25 %>/-
	</h3><hr>

	<form action="/cart/deleteAllInCart" method="post">
		<input type="submit" value="Place order" class="btn">

	</form>
	<%
	}
	%>
	</div>
</body>
</html>