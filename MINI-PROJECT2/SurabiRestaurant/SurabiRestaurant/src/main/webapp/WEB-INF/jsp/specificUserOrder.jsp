<%@page import="java.util.Iterator"%>
<%@page import="com.greatlearning.miniproject.bean.Orders"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Orders</title>
<style type="text/css">
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
         table {
        transform: translate(-0%, -1%);
        border-collapse: collapse;
        width: 800px;
        height: 10px;
        border: 1px solid #bdc3c7;
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    } 
     tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }  
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }                    
</style>
</head>
<body>
	
	<div align="center">
		
		<h1>SURABI RESTAURANT</h1>
		<hr>
	<div align="center">
		<a href="/admin/adminHome"><button class="btn1">Home</button></a>
		<a href="/admin/adminLogout"><button class="btn">Logout</button></a>
	</div>
	<hr>		 
	</div>
<div align="center">
	<h2>Orders History</h2>
	<form action="/orders/specificUserOrder" method="post">
		<label> E-Mail</label> <input type="email" name="email" required>
		<br />
		<br /> <input type="submit" value="Submit" class="btn2"> <br />
		<br />
	</form>
	<hr>
	
	<%
Object obj= request.getAttribute("objSpecificUserOrder");
Object obj2= request.getAttribute("fromSubmitted");
if(obj!=null){
		List<Orders> listOfOrder =(List<Orders>)obj;
		if(listOfOrder.isEmpty()){
			out.print("<h4>0- Orders, Order Now</h4>");
		}
		else{

			Iterator<Orders> ii = listOfOrder.iterator();
			int slNo=1;
			%>
	<h2>Orders History</h2>
	<table>
		<tr>
			<th>S.No:</th>
			<th>ID</th>
			<th>Name</th>
			<th>Plates</th>
			<th>Amount</th>
			<th>Ordered Date</th>
			

		</tr>
		<%
					while(ii.hasNext()){
						Object object = ii.next();
						Orders o = (Orders) object;
						
		%>
		<tr>
			<td><%=slNo++ %></td>
			<td><%=o.getKey().getItemId() %></td>
			<td><%=o.getItemName() %></td>
			<td><%= o.getNumberOfPlate()%></td>
			<td><%=o.getTotalPrice()%></td>
			<td><%=o.getKey().getDateAndTime() %></td>	
			
		</tr>
		<%
					}
		%>
	</table>
	<%
			
		}
			
		}


	%>

</div>
</body>
</html>