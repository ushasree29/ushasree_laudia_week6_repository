<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Home</title>


<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }                  
</style>
</head>
<body>

	<%
	Object loginResult = request.getAttribute("objLogInResult");
	if (loginResult != null) {
		out.print(loginResult);
	}
	%>
	
	<div align="center">
		<h1>SURABI RESTAURANT</h1>
		<hr>
		<div align="center">
		<a href="/admin/adminLogout"><button class="btn">Logout</button></a>
		</div>
		<hr>
		<h2>Admin Home page</h2>
	</div>

	<hr>
<div>
	<h3 >&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Menu CRUD Operations &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; User CRUD Operations</h3>
	&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	<a href="/menu/addNewMenuPage"><button class="btn1">Create Menu</button></a>
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	C- Create
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	<a href="/user/addNewUserPage"><button class="btn1">Create User</button></a>
	<br>
	<br>
	&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	<a href="/menu/displayMenuForAdmin"><button class="btn1">Read Menu</button ></a>
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	R- Read
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;
	<a href="/user/displayAllUser"><button class="btn1">Read Users</button></a>
	<br/>
	<br>
	&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	<a href="/menu/updateItemPricePage"><button class="btn1">Update Menu</button></a>
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	U- Update
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;
	<a href="/user/updateUserPasswordPage"><button class="btn1">Update User
			</button></a>
	<br/>
	<br>
	&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	<a href="/menu/deleteItemPage"><button class="btn1">Delete Menu</button></a>
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	D- Delete
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	<a href="/user/deleteUserPage"><button class="btn1">Delete User</button></a>
	</div>
	<hr>
	<a href="/orders/specificUserOrderPage" ><button class="btn1">Orders Of
			A User</button></a>
			<hr>
	<h3>Orders</h3>
	<a href="/orders/displayTodaysBills"><button class="btn1">Today's
			Orders</button></a>
	<a href="/orders/displayMonthlyBills"><button class="btn1">This Month's
			Orders</button></a>
	
	<hr>
</body>
</html>