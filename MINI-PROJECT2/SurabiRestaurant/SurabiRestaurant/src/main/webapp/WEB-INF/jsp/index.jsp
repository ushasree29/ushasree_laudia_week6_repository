<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>

<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }   
</style>
</head>
<body>
	<div align="center">	
		<h1>SURABI RESTAURANT</h1>
		<hr>
		<a href="/admin/adminLoginPage"><button class ="btn">ADMIN LOGIN</button></a><br>
		<br> 
		<a href="/user/userLoginPage"><button class ="btn">USER LOGIN</button></a>
	</div>
</body>
</html>