<%@page import="java.util.Iterator"%>
<%@page import="com.greatlearning.miniproject.bean.Menu"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Menu for Admin</title>
<style type="text/css">
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
     table {
        transform: translate(-0%, -5%);
        border-collapse: collapse;
        width: 500px;
        height: 10px;
        border: 1px solid #bdc3c7;
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    } 
     tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    } 
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
	
	<div align="center">

		<h1>SURABI RESTAURANT</h1>
		<hr>
	<div align="center">
		<a href="/admin/adminHome"><button class="btn1">Home</button></a>
		<a href="/admin/adminLogout"><button class="btn">Logout</button></a>
	</div>
	<hr>
	<h2>Menu</h2>
	<%Object menu = request.getAttribute("objMenu");
List<Menu> listOfMenu = (List<Menu>)menu;

if(listOfMenu.isEmpty()){
	out.print("<h3>No Menu to display</h3>");
}else{
	Iterator<Menu> ii = listOfMenu.iterator();
%>
	<table>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Price</th>

		</tr>

		<%
	 		while (ii.hasNext()){
	 			Menu item = ii.next();
	 			
%>
		<tr>
			<td><%=item.getItemId() %></td>
			<td><%=item.getItemName() %></td>
			<td><%= item.getItemPrice()%></td>

		</tr>

		<%
	 		}
	 
%>

	</table>

	<%
	
}
%>
</div>
<hr>
</body>
</html>