<%@page import="java.util.Iterator"%>
<%@page import="com.greatlearning.miniproject.bean.Menu"%>
<%@page import="java.util.List"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Menu For User</title>
<style type="text/css">
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
     table {
        top: 150%;
        transform: translate(-0%, -5%);
        border-collapse: collapse;
        width: 800px;
        height: 180px;
        border: 1px solid #bdc3c7;
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    } 
     tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }               
                   
</style>

</head>
<body>
	
	<div align="center">
		<h1>SURABI RESTAURANT</h1>
		<hr>
		<div align="center">
		<a href="/user/userlogout"><button class="btn">Logout</button><br></a>
		</div>
		<hr>
		<a href="/user/userHome"><button class="btn1">HOME</button></a> <a
			href="/cart/userCart"><button class="btn1">Cart</button></a> <br>
			<hr>
	<div class ="div">
	<h2> MENU</h2></div>
	<hr>
	&nbsp
	
	<%Object menu = session.getAttribute("objMenu");

     List<Menu> listOfMenu = (List<Menu>)menu;

     if(listOfMenu.isEmpty()){
	       out.print("<h4>No Menu to display</h3>");
     }else{
	       out.print("");
	       Iterator<Menu> ii = listOfMenu.iterator();
     %>
	<table>
		<tr><th></th>
			<th>ID</th>
			<th>Name</th>
			<th>Price</th>
			<th>Number of Plates</th>
			<th>Add to Cart</th>

		</tr>

		<%
	 		while (ii.hasNext()){
	 			Menu item = ii.next();
	 			
		%>
		<tr><form action="/cart/order" method="post">
			<td><input type="checkbox" value="select" name="item">
					<input type="hidden" value="<%=item.getItemId() %>" name="id">
					<input type="hidden" value="<%=item.getItemName() %>" name="name">
					<input type="hidden" value="<%= item.getItemPrice()%>" name="price">
			</td>
			<td><%=item.getItemId() %></td>
			<td><%=item.getItemName() %></td>
			<td><%= item.getItemPrice()%></td>			
			
				<td><input type="text"	name="numberOfPlates"></td>
				<td><input type="submit" value="Add" class="btn2"/></td>
			</form>
		</tr>

		<%
	 		}
	 
		%>

	</table>
	<br>
<%
}
%>
</div>
<hr>
<div align="center">
Status:
	<%
Object order=request.getAttribute("objSelect");
Object name = session.getAttribute("objName");
if(order!=null){
	out.print(order+"<br>");
}
%></div>
</body>
</html>