create database Surabi_usha29;

use Surabi_usha29;

CREATE TABLE admin(
email VARCHAR(100) PRIMARY KEY,
password VARCHAR(100)
);



CREATE TABLE user(
email VARCHAR(100) PRIMARY KEY,
password VARCHAR(100)
);

insert into admin
values ("laudia.ushasree29@gmail.com", "usha");

CREATE TABLE menu(
item_id int primary key,
item_name varchar(100),
item_price float
);

insert into menu value(1, "Idly", 50);
insert into menu value(2, "Dosa", 150);
insert into menu value(3, "Upma", 40);
insert into menu value(4, "Chapathi", 100);
insert into menu value(5, "Biriyani", 500);

CREATE TABLE my_cart(
email varchar(100),
item_id int,
item_name varchar(100),
number_of_plates int,
total_price float,
foreign key (email) references user(email),
foreign key (item_id) references menu(item_id),
primary key (email,item_id)
);

CREATE TABLE orders(
date_and_time varchar(50),
email varchar(100),
item_id int,
item_name varchar(100),
number_of_plate int,
total_price float,
foreign key (email) references user(email),
foreign key (item_id) references menu(item_id),
primary key (date_and_time,email,item_id)
);

select *from admin;
select *from menu;
select *from user;
select *from my_cart;
select *from orders;
